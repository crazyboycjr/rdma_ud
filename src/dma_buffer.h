#ifndef RDMA_DMA_BUFFER_H_
#define RDMA_DMA_BUFFER_H_

#include <infiniband/verbs.h>
#include <stdlib.h>

#include <type_traits>

#include "prism/logging.h"

namespace rdma {

// template <typename T>
// class DmaBuffer {};

/**
 * \brief DMA-capable buffer which carries a plain-old-type data
 */
template <typename T>
// class DmaBuffer<typename std::enable_if_t<std::is_pod_v<T>, T>> {
class DmaBuffer {
 public:
  DmaBuffer<T>() {}

  explicit DmaBuffer<T>(struct ibv_pd* pd, size_t size) {
    void* addr = malloc(size);
    PCHECK(mr_ = ibv_reg_mr(pd, addr, size, IBV_ACCESS_LOCAL_WRITE));
  }

  ~DmaBuffer() {
    void* addr = this->buf();
    if (mr_) {
      PCHECK(!ibv_dereg_mr(mr_));
      mr_ = nullptr;
    }
    free(addr);
  }

  inline struct ibv_mr* mr() const { return mr_; }
  inline void* buf() const { return mr_->addr; }
  inline size_t size() const { return mr_->length; }
  inline uint32_t send_bytes() const { return send_bytes_; }
  inline void set_send_bytes(uint32_t send_bytes) { send_bytes_ = send_bytes; }
  inline T data() const { return data; }
  inline void set_data(T data) { data_ = data; }

 private:
  // memory region
  struct ibv_mr* mr_;
  // length need to send
  uint32_t send_bytes_ = 0;
  // work request context associated with this buffer
  T data_;
};

}  // namespace rdma

#endif  // RDMA_DMA_BUFFER_H_