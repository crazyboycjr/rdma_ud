#ifndef RDMA_RDMA_CONTEXT_H_
#define RDMA_RDMA_CONTEXT_H_

#include "prism/utils.h"
#include "rdma/rdma_cma.h"


namespace rdma {

// here are some default values
const int kDefaultMaxSendWr = 128;
const int kDefaultMaxRecvWr = 128;
const int kDefaultMaxSendSge = 5;
const int kDefaultMaxRecvSge = 5;
const int kDefaultMaxInlineData = 128;
const int kDefaultRdmaListenBacklog = 512;
const int kDefaultRetryCount = 0;
const int kDefaultRnrRetryCount = 0;
const int kDefaultBufferSize = 65536;

class RdmaConfig {
  public:
  RdmaConfig() {}
  ~RdmaConfig() {}

  // clang-format off
  void BuildRdmaConfig() {
    max_send_wr_ = prism::GetEnvOrDefault<int>("RDMA_MAX_SEND_WR", kDefaultMaxSendWr);
    max_recv_wr_ = prism::GetEnvOrDefault<int>("RDMA_MAX_RECV_WR", kDefaultMaxRecvWr);
    max_send_sge_ = prism::GetEnvOrDefault<int>("RDMA_MAX_SEND_SGE", kDefaultMaxSendSge);
    max_recv_sge_ = prism::GetEnvOrDefault<int>("RDMA_MAX_RECV_SGE", kDefaultMaxRecvSge);
    max_inline_data_ = prism::GetEnvOrDefault<int>("RDMA_MAX_INLINE_DATA", kDefaultMaxInlineData);
    rdma_listen_backlog_ = prism::GetEnvOrDefault<int>("RDMA_RDMA_LISTEN_BACKLOG", kDefaultRdmaListenBacklog);
    retry_count_ = prism::GetEnvOrDefault<int>("RDMA_RETRY_COUNT", kDefaultRetryCount);
    rnr_retry_count_ = prism::GetEnvOrDefault<int>("RDMA_RNR_RETRY_COUNT", kDefaultRnrRetryCount);
    buffer_size_ = prism::GetEnvOrDefault<int>("RDMA_BUFFER_SIZE", kDefaultBufferSize);
  }
  // clang-format on

  int max_send_wr() const { return max_send_wr_; }
  int max_recv_wr() const { return max_recv_wr_; }
  int max_send_sge() const { return max_send_sge_; }
  int max_recv_sge() const { return max_recv_sge_; }
  int max_inline_data() const { return max_inline_data_; }
  int rdma_listen_backlog() const { return rdma_listen_backlog_; }
  int retry_count() const { return retry_count_; }
  int rnr_retry_count() const { return rnr_retry_count_; }
  int buffer_size() const { return buffer_size_; }

  private:
  int max_send_wr_;
  int max_recv_wr_;
  int max_send_sge_;
  int max_recv_sge_;
  int max_inline_data_;
  int rdma_listen_backlog_;
  int retry_count_;
  int rnr_retry_count_;
  int buffer_size_;
};

RdmaConfig* GetDefaultRdmaConfig();

class RdmaContext {
 public:
  RdmaContext();
  ~RdmaContext();

  inline RdmaConfig& config() { return config_; }
  inline ibv_pd* pd() { return pd_; }
  inline ibv_cq* send_cq() { return send_cq_; }
  inline ibv_cq* recv_cq() { return recv_cq_; }
  inline ibv_comp_channel* comp_channel() { return comp_channel_; }

 private:
  struct ibv_context* ctx_;
  struct ibv_pd* pd_;
  struct ibv_comp_channel* comp_channel_;
  struct ibv_cq* send_cq_;
  struct ibv_cq* recv_cq_;
  RdmaConfig config_;
};

}  // namespace rdma

#endif  // RDMA_RDMA_CONTEXT_H_