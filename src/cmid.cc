#include "./cmid.h"

#include "./rdma_context.h"
#include "prism/ibutil.h"

namespace rdma {

thread_local struct rdma_event_channel* CmId::event_channel_ = nullptr;

CmId::CmId(std::shared_ptr<RdmaContext> ctx, struct rdma_addrinfo* res,
           struct ibv_pd* pd, struct ibv_qp_init_attr* init_attr)
    : ctx_(ctx) {
  init_attr->qp_type = static_cast<enum ibv_qp_type>(res->ai_qp_type);
  init_attr->qp_context = this;
  PCHECK(!rdma_create_ep(&cm_id_, res, pd, init_attr));
}

CmId::CmId(std::shared_ptr<RdmaContext> ctx, struct rdma_cm_id* id)
    : cm_id_(id), ctx_(ctx) {}

CmId::~CmId() {
  if (cm_id_) {
    UnsetCqs();
    rdma_destroy_ep(cm_id_);
    cm_id_ = nullptr;
  }
}

CmId* CmId::Create(std::shared_ptr<RdmaContext> ctx, const RdmaAddrinfo& rai) {
  struct ibv_qp_init_attr init_attr;
  BuildQpInitAttr(ctx.get(), &init_attr);
  auto pd = ctx ? ctx->pd() : nullptr;
  CmId* cmid = new CmId(ctx, rai.rai, pd, &init_attr);
  cmid->SetCqs();
  return cmid;
}

void CmId::SetReuseAddr(bool reuse_addr) {
  int val = reuse_addr ? 1 : 0;
  PCHECK(!rdma_set_option(cm_id_, RDMA_OPTION_ID, RDMA_OPTION_ID_REUSEADDR,
                          &val, sizeof(val)));
}

void CmId::SetQpTos(uint8_t tos) {
  uint8_t val = tos;
  PCHECK(!rdma_set_option(cm_id_, RDMA_OPTION_ID, RDMA_OPTION_ID_TOS, &val,
                          sizeof(val)));
}

void CmId::SetAsync() {
  if (!event_channel_) {
    PCHECK(event_channel_ = rdma_create_event_channel());
  }
  PCHECK(!rdma_migrate_id(cm_id_, event_channel_));
}

void CmId::Listen(int backlog) { PCHECK(!rdma_listen(cm_id_, backlog)); }

CmId* CmId::GetRequest() {
  struct rdma_cm_id* id;
  PCHECK(!rdma_get_request(cm_id_, &id));
  CmId* new_cmid = new CmId(ctx_, id);
  new_cmid->SetCqs();
  return new_cmid;
}

void CmId::Accept(struct rdma_conn_param* conn_param) {
  struct rdma_conn_param default_conn_param;
  if (!conn_param) {
    BuildConnParam(ctx_.get(), &default_conn_param);
    conn_param = &default_conn_param;
  }
  PCHECK(!rdma_accept(cm_id_, conn_param));
}

void CmId::Connect(struct rdma_conn_param* conn_param) {
  struct rdma_conn_param default_conn_param;
  if (!conn_param) {
    BuildConnParam(ctx_.get(), &default_conn_param);
    conn_param = &default_conn_param;
  }
  PCHECK(!rdma_connect(cm_id_, conn_param));
}

void CmId::Disconnect() { PCHECK(!rdma_disconnect(cm_id_)); }

// template <typename T>
// void CmId::PostSend<T>(DmaBuffer<T>* buffer, int send_flags) {
//   buffer->set_cm_id(this);
//   PCHECK(!rdma_post_send(cm_id_, buffer, buffer->buf(), buffer->send_bytes(),
//                          buffer->mr(), send_flags));
// }

// template <typename T>
// void CmId::PostRecv<T>(DmaBuffer<T>* buffer) {
//   buffer->set_cm_id(this);
//   PCHECK(!rdma_post_recv(cm_id_, buffer, buffer->buf(), buffer->size(),
//                          buffer->mr()));
// }

#define CHECK_WC_SUCCESS(wc)                                                   \
  CHECK_EQ(wc->status, IBV_WC_SUCCESS) << prism::FormatString(                 \
      "CQ::  wr_id=0x%lx, wc_opcode=%s[%d], wc_status=%s[%d], wc_flag=0x%x\n " \
      "     byte_len=%u, immdata=%u, qp_num=0x%x, src_qp=%u\n",                \
      wc->wr_id, prism::ibutil::ibv_wc_opcode_string(wc->opcode), wc->opcode,  \
      ibv_wc_status_str(wc->status), wc->status, wc->wc_flags, wc->byte_len,   \
      wc->imm_data, wc->qp_num, wc->src_qp)

void CmId::GetSendComp(struct ibv_wc* wc) {
  CHECK(!ctx_) << "GetSendComp assumes non-shared cq mode";
  CHECK(cm_id_->send_cq_channel);
  PCHECK(1 == rdma_get_send_comp(cm_id_, wc));
  CHECK_WC_SUCCESS(wc);
}

void CmId::GetRecvComp(struct ibv_wc* wc) {
  CHECK(!ctx_) << "GetRecvComp assumes non-shared cq mode";
  CHECK(cm_id_->recv_cq_channel);
  PCHECK(1 == rdma_get_recv_comp(cm_id_, wc));
  CHECK_WC_SUCCESS(wc);
}

int CmId::PollSendComp(int num_entries, struct ibv_wc* wc) {
  int r = 0;
  PCHECK((r = ibv_poll_cq(cm_id_->send_cq, num_entries, wc)) >= 0);
  return r;
}

int CmId::PollRecvComp(int num_entries, struct ibv_wc* wc) {
  int r = 0;
  PCHECK((r = ibv_poll_cq(cm_id_->recv_cq, num_entries, wc)) >= 0);
  return r;
}

void CmId::BuildQpInitAttr(RdmaContext* ctx,
                           struct ibv_qp_init_attr* init_attr) {
  const RdmaConfig& conf = ctx ? ctx->config() : *GetDefaultRdmaConfig();

  memset(init_attr, 0, sizeof(*init_attr));
  // we set these two fields (qp_type, qp_context) later when we have enough context
  // see CmId::CmId(...)
  init_attr->sq_sig_all = 0;
  if (ctx) {
    init_attr->send_cq = ctx->send_cq();
    init_attr->recv_cq = ctx->recv_cq();
  }

  init_attr->cap.max_send_wr = conf.max_send_wr();
  init_attr->cap.max_recv_wr = conf.max_recv_wr();
  init_attr->cap.max_send_sge = conf.max_send_sge();
  init_attr->cap.max_recv_sge = conf.max_recv_sge();
  init_attr->cap.max_inline_data = conf.max_inline_data();
}

void CmId::BuildConnParam(RdmaContext* ctx,
                          struct rdma_conn_param* conn_param) {
  const RdmaConfig& conf = ctx ? ctx->config() : *GetDefaultRdmaConfig();

  memset(conn_param, 0, sizeof(*conn_param));
  conn_param->retry_count = conf.retry_count();
  conn_param->rnr_retry_count = conf.rnr_retry_count();
}

void CmId::SetCqs() {
  if (ctx_) {
    CHECK((!cm_id_->send_cq || cm_id_->send_cq == ctx_->send_cq()) &&
          (!cm_id_->recv_cq || cm_id_->recv_cq == ctx_->recv_cq()));
    cm_id_->send_cq = ctx_->send_cq();
    cm_id_->recv_cq = ctx_->recv_cq();
    cm_id_->send_cq_channel = ctx_->comp_channel();
    cm_id_->recv_cq_channel = ctx_->comp_channel();
  }
}

void CmId::UnsetCqs() {
  if (ctx_) {
    // we are using shared cq mode
    cm_id_->send_cq = nullptr;
    cm_id_->recv_cq = nullptr;
    cm_id_->send_cq_channel = nullptr;
    cm_id_->send_cq_channel = nullptr;
  }
}

}  // namespace rdma