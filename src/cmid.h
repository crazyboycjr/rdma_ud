#ifndef RDMA_CMID_H_
#define RDMA_CMID_H_

#include <netdb.h>
#include <rdma/rdma_verbs.h>

#include <memory>
#include <string>

#include "./dma_buffer.h"
#include "./rdma_context.h"
#include "prism/logging.h"
#include "prism/utils.h"

namespace rdma {

struct RdmaAddrinfo {
  struct rdma_addrinfo* rai;

  RdmaAddrinfo(uint16_t port, enum rdma_port_space port_space) {
    struct rdma_addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = RAI_PASSIVE;
    hints.ai_port_space = port_space;
    {
      int err =
          rdma_getaddrinfo(nullptr, std::to_string(port).c_str(), &hints, &rai);
      CHECK(!err) << "rdma_getaddrinfo: " << gai_strerror(err);
    }
  }
  RdmaAddrinfo(const char* host, uint16_t port, enum rdma_port_space port_space,
               bool passive) {
    struct rdma_addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = passive ? RAI_PASSIVE : 0;
    hints.ai_port_space = port_space;
    {
      int err =
          rdma_getaddrinfo(host, std::to_string(port).c_str(), &hints, &rai);
      CHECK(!err) << "rdma_getaddrinfo: " << gai_strerror(err);
    }
  }
  RdmaAddrinfo(const char* host, uint16_t port, enum rdma_port_space port_space)
      : RdmaAddrinfo(host, port, port_space, false) {}
  ~RdmaAddrinfo() {
    if (rai) rdma_freeaddrinfo(rai);
  }
  static std::string AddrStr(struct sockaddr* addr, socklen_t len) {
    char addr_buf[NI_MAXHOST];
    char serv_buf[NI_MAXSERV];
    int flags = NI_NUMERICHOST | NI_NUMERICSERV;
    {
      int err = getnameinfo(addr, len, addr_buf, sizeof(addr_buf), serv_buf,
                            sizeof(serv_buf), flags);
      CHECK(!err) << "getnameinfo: " << gai_strerror(err);
    }
    return prism::FormatString("%s:%s", addr_buf, serv_buf);
  }
  std::string AddrStr() const {
    struct sockaddr* addr = nullptr;
    socklen_t len = 0;
    if (CHECK_NOTNULL(rai)->ai_flags & RAI_PASSIVE) {
      addr = rai->ai_src_addr;
      len = rai->ai_src_len;
    } else {
      addr = rai->ai_dst_addr;
      len = rai->ai_dst_len;
    }
    return AddrStr(addr, len);
  }
};

// rdma_create_ep is convenient, but it cannot cooperate with
// rdma_set_option freely. Need to change to rdma_create_id later.
class CmId {
 private:
  static thread_local struct rdma_event_channel* event_channel_;

 public:
  CmId(std::shared_ptr<RdmaContext> ctx, struct rdma_cm_id* id);
  CmId(std::shared_ptr<RdmaContext> ctx, struct rdma_addrinfo* res,
       struct ibv_pd* pd, struct ibv_qp_init_attr* init_attr);
  ~CmId();

  static CmId* Create(std::shared_ptr<RdmaContext> ctx,
                      const RdmaAddrinfo& rai);

  /**
   * Reuse must be set before an address is bound to the id.
   */
  void SetReuseAddr(bool reuse_addr);

  /**
   * The service type should be specified before performing
   * route resolution, as existing communication on the connection
   * identifier may be unaffected.
   */
  void SetQpTos(uint8_t tos);

  void SetAsync();

  void Listen(int backlog);

  CmId* GetRequest();

  void Accept(struct rdma_conn_param* conn_param = nullptr);

  void Connect(struct rdma_conn_param* conn_param = nullptr);

  void Disconnect();

  template <typename T>
  void PostSend(DmaBuffer<T>* buffer, T data, int send_flags = 0) {
    buffer->set_data(data);
    PCHECK(!rdma_post_send(cm_id_, buffer, buffer->buf(), buffer->send_bytes(),
                           buffer->mr(), send_flags));
  }

  template <typename T>
  void PostSendUd(DmaBuffer<T>* buffer, T data, struct ibv_ah* ah,
                  uint32_t remote_qpn, int send_flags = 0) {
    buffer->set_data(data);
    PCHECK(!rdma_post_ud_send(cm_id_, buffer, buffer->buf(),
                              buffer->send_bytes(), buffer->mr(), send_flags,
                              ah, remote_qpn));
  }

  template <typename T>
  void PostRecv(DmaBuffer<T>* buffer, T data) {
    buffer->set_data(data);
    PCHECK(!rdma_post_recv(cm_id_, buffer, buffer->buf(), buffer->size(),
                           buffer->mr()));
  }

  void GetSendComp(struct ibv_wc* wc);

  void GetRecvComp(struct ibv_wc* wc);

  int PollSendComp(int num_entries, struct ibv_wc* wc);

  int PollRecvComp(int num_entries, struct ibv_wc* wc);

  std::string GetPeerAddrStr() {
    return RdmaAddrinfo::AddrStr(rdma_get_peer_addr(cm_id_),
                                 sizeof(struct sockaddr_storage));
  }

  std::string GetLocalAddrStr() {
    return RdmaAddrinfo::AddrStr(rdma_get_local_addr(cm_id_),
                                 sizeof(struct sockaddr_storage));
  }

  inline struct rdma_cm_id* cm_id() const {
    return cm_id_;
  }

  inline RdmaContext* ctx() const {
    return ctx_.get();
  }

 private:
  static void BuildQpInitAttr(RdmaContext* ctx,
                              struct ibv_qp_init_attr* init_attr);
  static void BuildConnParam(RdmaContext* ctx,
                             struct rdma_conn_param* conn_param);

  void SetCqs();
  void UnsetCqs();

  /// COMMENT(cjr): We are using shared_ptr here not because we expect
  /// RdmaContext to expire. Instead, we are just emphasising that ctx_ is
  /// held by multiple CmId instances.
  ///
  /// If ctx is not null, then we are in the shared cq mode. That means, we have
  /// to set the cm_id->send/recv_cq and cm_id->send/recv_cq_channel manually,
  /// and unset them before destructing.
  std::shared_ptr<RdmaContext> ctx_;
  struct rdma_cm_id* cm_id_;
};

}  // namespace rdma

#endif  // RDMA_CMID_H_
