cmake_minimum_required(VERSION 3.0)

# the executable
add_executable(rdma_ud main.cc cmid.cc rdma_context.cc)
target_link_libraries(rdma_ud
    rdmacm
    ibverbs
)
