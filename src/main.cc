#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include <functional>
#include <iostream>
#include <memory>
#include <optional>

#include "./cmid.h"
#include "./dma_buffer.h"
#include "./rdma_context.h"
#include "prism/logging.h"

const int kDefaultPort = 18000;
const size_t kDefaultDataSize = 0;

struct Opts {
  bool is_server;
  std::optional<std::string> host;
  size_t data_size{kDefaultDataSize};
  uint16_t port{kDefaultPort};

  std::string DebugString() {
    const char* NULLOPT = "(nullopt)";
    std::stringstream ss;
    ss << "{ is_server: " << is_server << ", host: " << host.value_or(NULLOPT)
       << ", port: " << port << ", data_size: " << data_size << " }";
    return ss.str();
  }
};

void Usage(const char* app) {
  // clang-format off
  fprintf(stdout, "Usage:\n");
  fprintf(stdout, "  %s           start an RPC benchmark server\n", app);
  fprintf(stdout, "  %s <host>    connect to server at <host>\n", app);
  fprintf(stdout, "\nOptions:\n");
  fprintf(stdout, "  -p, --port=<int>    the port to listen on or connect to, (default %d)\n", kDefaultPort);
  fprintf(stdout, "  -d, --data=<size>   additional data size per request, (default %ld)\n", kDefaultDataSize);
  // clang-format on
}

int ParseArgument(int argc, char* argv[], Opts* opts) {
  int err = 0;
  static struct option long_options[] = {// clang-format off
      {"help", no_argument, 0, 'h'},
      {"port", required_argument, 0, 'p'},
      {"data", required_argument, 0, 'd'},
      {0, 0, 0, 0}
  };  // clang-format on
  while (1) {
    int option_index = 0, c;
    c = getopt_long(argc, argv, "hp:a:r:P:d:t:", long_options, &option_index);
    if (c == -1) break;
    switch (c) {
      case 'h': {
        Usage(argv[0]);
        exit(0);
      } break;
      case 'p': {
        opts->port = std::stoi(optarg);
      } break;
      case 'd': {
        opts->data_size = std::stoi(optarg);
      } break;
      case '?':
      default:
        err = 1;
        goto out1;
    }
  }
  if (optind < argc) {
    if (optind + 1 != argc) {
      err = 1;
      goto out1;
    }
    opts->is_server = false;
    opts->host = argv[optind];
  } else {
    opts->is_server = true;
  }
out1:
  return err;
}

#include <prism/ibutil.h>
#include <prism/utils.h>
#define CHECK_WC_SUCCESS(wc)                                                   \
  CHECK_EQ(wc->status, IBV_WC_SUCCESS) << prism::FormatString(                 \
      "CQ::  wr_id=0x%lx, wc_opcode=%s[%d], wc_status=%s[%d], wc_flag=0x%x\n " \
      "     byte_len=%u, immdata=%u, qp_num=0x%x, src_qp=%u\n",                \
      wc->wr_id, prism::ibutil::ibv_wc_opcode_string(wc->opcode), wc->opcode,  \
      ibv_wc_status_str(wc->status), wc->status, wc->wc_flags, wc->byte_len,   \
      wc->imm_data, wc->qp_num, wc->src_qp)

namespace rdma {

void SendHello(CmId* cm_id, uint32_t remote_qpn, struct ibv_ah* ah) {
  auto buffer = std::make_unique<DmaBuffer<int>>(cm_id->ctx()->pd(),
                                                 1024 + sizeof(struct ibv_grh));
  strcpy(static_cast<char*>(buffer->buf()), "Hello");
  buffer->set_send_bytes(6);
  cm_id->PostSendUd(buffer.get(), 0, ah, remote_qpn, IBV_SEND_SIGNALED);

  struct ibv_wc wc;
  while (0 == cm_id->PollSendComp(1, &wc))
    ;
  CHECK_WC_SUCCESS((&wc));
  LOG(INFO) << "send done";
}

}  // namespace rdma

void RunServer(Opts opts) {
  using namespace rdma;
  auto ctx = std::make_shared<RdmaContext>();
  RdmaAddrinfo rai(opts.port, RDMA_PS_UDP);
  auto listener_id = std::unique_ptr<CmId>(CmId::Create(ctx, rai));
  LOG(INFO) << "listening on addr: " << rai.AddrStr();
  listener_id->Listen(128);
  auto cm_id = std::unique_ptr<CmId>(listener_id->GetRequest());

  // post recvs before accept
  auto buffer = std::make_unique<DmaBuffer<int>>(ctx->pd(),
                                                 1024 + sizeof(struct ibv_grh));
  cm_id->PostRecv(buffer.get(), 0);

  cm_id->Accept();

  // COMMENT(cjr): The connection establishment of UD type does not have 3
  // handshakes. Unlike RC, UD qp will never receive an
  // RDMA_CM_EVENT_ESTABLISHED event after rdma_accept(). Therefore, only the
  // active side can get ibv_ah and remote_qpn from the passive side. An extra
  // information exchange is needed if bi-directional communication is desired.
  //
  // Therefore, we have to use ibv_create_ah_from_wc() to get the ibv_ah.

  // recv
  struct ibv_wc wc;
  while (0 == cm_id->PollRecvComp(1, &wc))
    ;
  CHECK_WC_SUCCESS((&wc));
  LOG(INFO) << "received " << wc.byte_len << " bytes";  // 40 Bytes for GRH
  LOG(INFO) << "data: "
            << (static_cast<char*>(buffer->buf()) + sizeof(struct ibv_grh));

  auto grh = static_cast<struct ibv_grh*>(buffer->buf());
  auto remote_qpn = wc.src_qp;
  auto ah = std::unique_ptr<struct ibv_ah, std::function<void(struct ibv_ah*)>>(
      ibv_create_ah_from_wc(ctx->pd(), &wc, grh, cm_id->cm_id()->port_num),
      [](struct ibv_ah* ah) { ibv_destroy_ah(ah); });
  PCHECK(ah) << "ibv_create_ah_from_wc";

  LOG(INFO) << "my_qpn: " << cm_id->cm_id()->qp->qp_num;
  LOG(INFO) << "remote_qpn: " << remote_qpn;

  // send
  SendHello(cm_id.get(), remote_qpn, ah.get());
}

void RunClient(Opts opts) {
  using namespace rdma;
  auto ctx = std::make_shared<RdmaContext>();
  RdmaAddrinfo rai(opts.host->c_str(), opts.port, RDMA_PS_UDP);
  LOG(INFO) << "connecting to addr: " << rai.AddrStr();
  auto cm_id = std::unique_ptr<CmId>(CmId::Create(ctx, rai));

  // post recvs before connect
  auto buffer = std::make_unique<DmaBuffer<int>>(
      ctx->pd(), 1024 + sizeof(struct ibv_grh));
  cm_id->PostRecv(buffer.get(), 1);

  cm_id->Connect();

  // at this moment, new_id->cm_id()->event should contains a
  // RDMA_CM_EVENT_CONNECT_ESTABLISHED
  auto cm_event = cm_id->cm_id()->event;
  CHECK(cm_event) << "cm_event is null";
  CHECK(cm_event->event == RDMA_CM_EVENT_ESTABLISHED)
      << rdma_event_str(cm_event->event);

  auto& ud_param = cm_event->param.ud;
  auto remote_qpn = ud_param.qp_num;
  LOG(INFO) << "my_qpn: " << cm_id->cm_id()->qp->qp_num;
  LOG(INFO) << "remote_qpn: " << remote_qpn;

  auto ah = std::unique_ptr<struct ibv_ah, std::function<void(struct ibv_ah*)>>(
      ibv_create_ah(ctx->pd(), &ud_param.ah_attr),
      [](struct ibv_ah* ptr) { ibv_destroy_ah(ptr); });
  PCHECK(ah) << "ibv_create_ah";

  // send
  SendHello(cm_id.get(), remote_qpn, ah.get());

  // recv
  {
    struct ibv_wc wc;
    while (0 == cm_id->PollRecvComp(1, &wc))
      ;
    CHECK_WC_SUCCESS((&wc));

    LOG(INFO) << "received " << wc.byte_len << " bytes";  // 40 Bytes for GRH
    LOG(INFO) << "data: "
              << (static_cast<char*>(buffer->buf()) + sizeof(struct ibv_grh));
  }
}

int main(int argc, char* argv[]) {
  LOG(INFO) << "Program start";

  Opts opts;
  if (ParseArgument(argc, argv, &opts)) {
    Usage(argv[0]);
    return 1;
  }

  std::cout << opts.DebugString() << std::endl;

  if (opts.is_server) {
    RunServer(std::move(opts));
  } else {
    RunClient(std::move(opts));
  }

  return 0;
}