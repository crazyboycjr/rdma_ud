#include "./rdma_context.h"

#include <mutex>

namespace rdma {

RdmaConfig* GetDefaultRdmaConfig() {
  static RdmaConfig conf;
  static std::once_flag flag;
  std::call_once(flag, []() { conf.BuildRdmaConfig(); });
  return &conf;
}

RdmaContext::RdmaContext() {
  int num_devices;
  auto contexts = rdma_get_devices(&num_devices);
  CHECK_GT(num_devices, 0)
      << "Failed to get any rdma contexts, please check your RDMA settings.";

  std::string device_name = prism::GetEnvOrDefault("MLT_RDMA_DEVICE", "mlx5_0");

  auto found = false;
  for (int i = 0; i < num_devices; i++) {
    auto ctx = contexts[i];
    if (std::string(ibv_get_device_name(ctx->device)) == device_name) {
      ctx_ = ctx;
      found = true;
      break;
    }
  }

  CHECK(found) << "Failed to find " << device_name;

  pd_ = ibv_alloc_pd(ctx_);
  CHECK(pd_) << "ibv_alloc_pd failed";

  // TODO(cjr): Do I have to create separate channels?
  comp_channel_ = ibv_create_comp_channel(ctx_);
  CHECK(comp_channel_) << "ibv_create_comp_channel failed";

  send_cq_ = ibv_create_cq(ctx_, 8 * 128, nullptr, comp_channel_, 0);
  recv_cq_ = ibv_create_cq(ctx_, 8 * 128, nullptr, comp_channel_, 0);
  CHECK(send_cq_ && recv_cq_) << "ibv_create_cq failed";

  config_.BuildRdmaConfig();
}

RdmaContext::~RdmaContext() {
  PCHECK(!ibv_destroy_cq(recv_cq_));
  PCHECK(!ibv_destroy_cq(send_cq_));

  PCHECK(!ibv_destroy_comp_channel(comp_channel_));

  PCHECK(!ibv_dealloc_pd(pd_));
}

}  // namespace rdma