.PHONY: clean clean_all

CXX := g++
CC := gcc
INCPATH := -I./include -I./src
CFLAGS := -g -Wall -Werror -std=c++20
LDFLAGS := -lpthread

DEPS_PATH ?= $(shell pwd)/deps

DEBUG := 1
ifeq ($(DEBUG), 1)
	CFLAGS += -fsanitize=address -ftrapv -fstack-protector-strong -DPRISM_LOG_STACK_TRACE -DPRISM_LOG_STACK_TRACE_SIZE=128
else
	CFLAGS += -O2 -DNDEBUG
endif

APP := rdma_ud

SRCS := $(wildcard src/*.cc)
OBJS := $(patsubst src/%,build/%,$(SRCS:.cc=.o))

all: $(APP)

mlt: build/libmlt.a

build/libmlt.a: $(OBJS)
	ar crv $@ $(filter %.o, $?)

build/%.o: src/%.cc
	@mkdir -p $(@D)
	$(CXX) $(INCPATH) -std=c++20 -MM -MT build/$*.o $< >build/$*.d
	$(CXX) $(INCPATH) $(CFLAGS) -c $< -o $@

-include build/*.d
-include build/*/*.d

clean:
	rm -rfv $(OBJS) $(APP:=.o)
	rm -rfv $(OBJS:.o=.d) $(APP:=.d)

clean_all: clean
	rm -rf $(DEPS_PATH)
